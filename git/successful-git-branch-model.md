
# A successful Git branching model :tada: :tada: :tada: 

Author: [Vincent Driessen](https://nvie.com/about/)

Mô hình này nguyên bản được đưa ra bởi **Vincent Driessen** được dịch và tóm tắt theo bản ghi nhớ, chi tiết tại [đây](https://nvie.com/posts/a-successful-git-branching-model)

<img src="../assets/git-model@2x.png" width=650/>

Theo mô hình này, khi sử dụng `git` để quản lý code, git project phải được chia thành nhiều `branch` (nhánh) gồm: `master`, `develop`, `release-*`, `hotfix-*` và các *feature branch*. Việc merge các nhánh được thực hện thông qua việc tạo `merge request`. User có quyền  *Owner* hoặc *Maintainer* là người trực tiếp duyệt code và thực hiện việc merge.

## Các nhánh chính

Các nhánh chính luôn luôn được bảo vệ để tránh commit trực tiếp (**protected branch** trong gitlab), chỉ được thay đổi bằng cách merge từ nhánh khác và tồn tại mãi mãi theo dòng thời gian phát triển của project bao gồm:

- `master` 
- `develop`

Chúng ta xem nhánh `origin/master` như nhánh luôn luôn giữ *latest production state*, còn nhánh `orgin/develop` là nhánh giữ *latest development state*. Khi code trên nhánh `develop` có thể sẵn sàng để release thì chúng sẽ được merge vào nhánh `master` và được gắn 1 tag với *release number*. Do đó mỗi khi nhánh `master` thay đổi có nghĩa là 1 bản release sẽ được tung ra. Chúng ta cần tuân thủ chặt chẽ luật này, để có thể cấu hình `Auto DevOps`

## Các nhánh còn lại

Ngoài các nhánh chính, trong quá trình phát triển, mô hình này sử dụng các nhánh khác nhau hỗ trợ việc phát triển song song của các thành viên trong team, dễ dàng theo dõi các tính năng, chuẩn bị để release production, hoặc sửa lỗi trên production. Các nhánh này chỉ tồn tại trong 1 khoảng thời gian nhất định của chu trình phát triển phần mềm vì chính sẽ bị xóa đi.

### Nhánh *Feature*

- Được tách nhánh từ `develop`
- Merge ngược về `develop`
- Quy ước đặt tên: ngắn gọn chỉ tính năng đang hiện thực, ngoại trừ `master`, `develop`, `release-*`, `hotfix-*`

Nhánh này được sử dụng để phát triển các tính năng mới của phần mềm. Khi phát triển các tính năng mới, nhánh này sẽ được tạo từ `develop` và sẽ tồn tại miễn khi tính năng đó còn phát triển.

### Nhánh `release`

- Được tách nhánh từ `develop`
- Merge ngược về `develop` **và** `master`
- Quy ước đặt tên: `release-*`

Nhánh `release` được sử dụng để sửa 1 số lỗi nhỏ ở `develop` hoặc thay đổi số build, bản build có thể có trong phần mềm, chuẩn bị để release 1 số các tính năng có trong production bản tiếp theo

### Nhánh `hotfix`

- Được tách nhánh từ `master`
- Merge ngược về `develop` **và** `master`
- Quy ước đặt tên: `hotfix-*`

Nhánh `hot-fix` được dùng để sửa các lỗi khẩn cấp trên production mà không thể chờ đợi các bản build tiếp theo release được. Việc sử dụng nhánh này giúp vài thành viên tiếp tục phát triển và vài thành viên trong team có thể sửa các lỗi ở production mà không ảnh hướng đến việc phát triển phần mềm.

Có 1 ngoại lệ, nếu có 1 nhánh `release` đang tồn tại thì phải merge vào nhánh `release` thay vì nhánh `develop`. Ngoài ra nếu ở nhánh `develop` đang cần ngay lập tức việc sửa lỗi để phát triển các tính năng tiếp theo thì có thể merge vào nhánh `develop` mặc dù nhánh `release` đang tồn tại.
